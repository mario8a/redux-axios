import { Search } from './Search';


export const Nav = () => {

  return (
    <nav>
      <h3>Nav</h3>
      <Search />
    </nav>
  )
};
