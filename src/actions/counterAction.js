const incrementActions = () => {
  return {
    type: 'INCREMENT'
  }
}

export default incrementActions;