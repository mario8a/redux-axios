import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";



export const Search = () => {

  const counter = useSelector(state => state.counter);
  const dispatch = useDispatch();

  return (
    <div>
      <input type="text" />
      <h1 >Hey {counter}</h1>
      <button onClick={() => dispatch({type: 'INCREMENT'})}>Increment</button>
    </div>
  );
};
