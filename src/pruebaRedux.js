import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore } from 'redux';

// STORE -> GLOBALIZED STATE


// ACTION - Describe what i do
const increment = () => {
  return {
    type: 'INCREMENT'
  };
};

//REDUCER - Filter the actions and modify the state
const counterReducer = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default:
      return state;  
  }
};

//DISPATCH - fire an action
// dispatch('INCREMENT')

let store = createStore(counterReducer);

// this is not how we are gonna do it in react.... but whatever :)
store.subscribe(() => console.log(store.getState()));
store.dispatch(increment());

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
