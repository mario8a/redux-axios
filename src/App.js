import { useSelector, useDispatch } from 'react-redux';
import { Nav } from './Nav';
import './App.css';
// useSelector -> Extractt everuthing in redux store


function App() {

  const counter = useSelector(state => state.counter);
  const signIn = useSelector(state => state.isLogged);
  const dispatch = useDispatch();
  // dispatch send the type to the reducer

  return (
    <div className="App">
      <h1>Counter: {counter}</h1>
      <button onClick={() => dispatch({type: 'SIGN_IN'})}>Sign in</button>
      <Nav />
      {
        signIn && <h1>Movie LIST</h1>
      }
    </div>
  );
}

export default App;
